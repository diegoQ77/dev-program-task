from datetime import datetime

FORMAT_SHORT_YEAR = '%m/%d/%y, %I:%M:%S %p'
FORMAT_LONG_YEAR = '%m/%d/%Y, %I:%M:%S %p'


class DateTimeWrapper:
    """ This class wraps native a native `datetime` """
    
    def __init__(self, datetime_str, format=FORMAT_SHORT_YEAR):
        """
        This constructs a DateTimeWrapper object
        :param datetime_str: A str containing datetime value
        :param format: A str that holds the format of the datetime_str
        """
        self.__native_datetime = datetime.strptime(datetime_str, format)

    @property
    def datetime(self):
        return self.__native_datetime


def calculate_duration_hh_mm_ss(start_dtw, end_dtw):
    """
    This function calculates duration between to datetime values
    :param start_dt: A DateTimeWrapper object
    :param end_dt: A DateTimeWrapper object
    :return: A tuple that holds hours, minutes, seconds values
    """
    if start_dtw is None or end_dtw is None:
        raise ValueError("start_dt or end_dt cannot be None")

    if not isinstance(start_dtw, DateTimeWrapper) or not isinstance(end_dtw, DateTimeWrapper):
        raise ValueError("start_dt and end_dt must be DateTimeWrapper instances")
    
    diff = end_dtw.datetime - start_dtw.datetime 
    seconds = diff.total_seconds() 
    mm, ss = divmod(seconds, 60)
    hh, mm = divmod(mm, 60)
    return int(hh), int(mm), int(ss)
