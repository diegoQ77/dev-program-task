import pytest
from src.teams_meeting import Duration

def test_duration_datetimes():
    d1 = Duration(0, 28, 40)
    d2 = Duration(0, 8, 47)
    assert d2 < d1, "expected d2 less than d1"
    
    d = Duration(0, 0, 5)
    assert str(d) == "5s", f"expected d str equals to 5s, {d}"
    
    d = Duration(1, 26, 55)
    assert str(d) == "1h 26m 55s", "expected d str equals to 1h 26m 55s"
    
    d = Duration(0, 1, 26)
    assert str(d) == "1m 26s", "expected d str equals to 1m 26s"
    
    d = Duration(0, 1, 26)
    assert str(d) == "1m 26s", "expected d str equals to 1m 26s"
    
    d1 = Duration(0, 28, 40)
    d2 = Duration(0, 8, 47)
    assert d1 > d2, "expected d1 greater than d2"
    
    d1 = Duration(0, 28, 40)
    d2 = Duration(0, 8, 47)
    assert d2 < d1, "expected d2 less than d1"
    
    d1 = Duration(0, 28, 40)
    d2 = Duration(0, 28, 40)
    assert d2 == d1, "expected d2 is equal to d1"
    
    d1 = Duration(0, 28, 40)
    d2 = Duration(0, 8, 47)
    assert d2 != d1, "d1 and d2 are not equals"
    

def test_duration_datetimes_sorted_ascendent():
    duration_list =  [Duration(0, 8, 47), Duration(0, 28, 40), Duration(0, 1, 0)]
    sorted_duration_list = sorted(duration_list)
    assert sorted_duration_list[0].hours == 0 and sorted_duration_list[0].minutes == 1 and sorted_duration_list[0].seconds == 0
    assert sorted_duration_list[-1].hours == 0 and sorted_duration_list[-1].minutes == 28 and sorted_duration_list[-1].seconds == 40
        
def test_duration_datetimes_sorted_descendent():
    duration_list =  [Duration(0, 8, 47), Duration(0, 28, 40), Duration(0, 1, 0)]
    sorted_duration_list = sorted(duration_list, reverse=True)
    assert sorted_duration_list[0].hours == 0 and sorted_duration_list[0].minutes == 28 and sorted_duration_list[0].seconds == 40
    assert sorted_duration_list[-1].hours == 0 and sorted_duration_list[-1].minutes == 1 and sorted_duration_list[-1].seconds == 0        
        
