from src.helpers import DateTimeWrapper, calculate_duration_hh_mm_ss
FORMAT_SHORT_YEAR = '%m/%d/%y, %I:%M:%S %p'
FORMAT_LONG_YEAR = '%m/%d/%Y, %I:%M:%S %p'
import unittest


class TestHelpers(unittest.TestCase):
    """ This class contains tests for `helpers` module functions"""

    def test_calculate_duration_hh_mm_ss_raise_value_error_case1(self):
        """ should raise Value error when either start_dt or end_dt is None """
        with self.assertRaises(ValueError) as cm:
            calculate_duration_hh_mm_ss(None, None)
        self.assertEqual(str(cm.exception), 'start_dt or end_dt cannot be None')

    def test_calculate_duration_hh_mm_ss_raise_value_error_case2(self):
        """ should raise Value error when either start_dt or end_dt is None """
        with self.assertRaises(ValueError) as cm:
            calculate_duration_hh_mm_ss(None, DateTimeWrapper('5/25/22, 10:30:53 AM', FORMAT_SHORT_YEAR))
        self.assertEqual(str(cm.exception), 'start_dt or end_dt cannot be None')

    def test_calculate_duration_hh_mm_ss_raise_value_error_case3(self):
        """ should raise Value error when either start_dt or end_dt is None """
        with self.assertRaises(ValueError) as cm:
            calculate_duration_hh_mm_ss(DateTimeWrapper('5/25/22, 10:30:53 AM', FORMAT_SHORT_YEAR), None)
        self.assertEqual(str(cm.exception), 'start_dt or end_dt cannot be None')

    def test_calculate_duration_hh_mm_ss_raise_value_error_case4(self):
        """ should raise Value error when either start_dt or end_dt are not DateTimeWrapper instance """
        with self.assertRaises(ValueError) as cm:
            calculate_duration_hh_mm_ss('5/25/22, 10:30:53 AM', '5/25/22, 11:39:17 AM')
        self.assertEqual(str(cm.exception), 'start_dt and end_dt must be DateTimeWrapper instances')

    def test_calculate_duration_hh_mm_ss_case1(self):
        start = DateTimeWrapper('5/25/22, 10:30:53 AM', FORMAT_SHORT_YEAR)
        end = DateTimeWrapper('5/25/22, 11:39:17 AM', FORMAT_SHORT_YEAR)
        hh, mm, ss = calculate_duration_hh_mm_ss(start, end)
        self.assertIsNotNone(hh)
        self.assertIsNotNone(mm)
        self.assertIsNotNone(ss)
        self.assertTrue(type(hh) is int)
        self.assertTrue(type(mm) is int)
        self.assertTrue(type(ss) is int)
        print(hh, mm, ss)
        self.assertTrue(hh >= 0)
        self.assertTrue(mm >= 0)
        self.assertTrue(ss >= 0)


unittest.main(argv=[''], verbosity=2, exit=False)