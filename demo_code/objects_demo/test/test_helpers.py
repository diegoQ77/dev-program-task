from datetime import datetime
import pytest
from src.helpers import DateTimeWrapper, calculate_duration_hh_mm_ss
FORMAT_SHORT_YEAR = '%m/%d/%y, %I:%M:%S %p'
FORMAT_LONG_YEAR = '%m/%d/%Y, %I:%M:%S %p'

def test_datetime_elemets():
    
    d = DateTimeWrapper('5/25/22, 10:30:53 AM')
    assert d.datetime.year == 2022 and d.datetime.month == 5 and d.datetime.day == 25
    d = DateTimeWrapper('5/25/2022, 10:30:53 AM', FORMAT_LONG_YEAR)
    assert d.datetime.year == 2022 and d.datetime.month == 5 and d.datetime.day == 25
    d = DateTimeWrapper(datetime.now().strftime(FORMAT_LONG_YEAR), FORMAT_LONG_YEAR)
    assert d.datetime.year == datetime.now().year and d.datetime.month == datetime.now().month
    d = DateTimeWrapper(datetime.now().strftime(FORMAT_LONG_YEAR), FORMAT_LONG_YEAR)
    assert d.datetime.year == datetime.now().year and d.datetime.month == datetime.now().month
    

def test_datetime_validation():

    with pytest.raises(expected_exception=ValueError, match="time data '5/25/2022, 10:30:53 AM' does not match format '%m/%d/%y, %I:%M:%S %p'"):
            DateTimeWrapper('5/25/2022, 10:30:53 AM')
            
    with pytest.raises(expected_exception=ValueError, match="time data '5/25/22, 10:30:53 AM' does not match format '%m/%d/%Y, %I:%M:%S %p'"):
            DateTimeWrapper('5/25/22, 10:30:53 AM', FORMAT_LONG_YEAR)
    
    
def test_datetune_in_hh_mm_ss():
    start_dt_w_obj = DateTimeWrapper("5/25/22, 10:30:53 AM", FORMAT_SHORT_YEAR)
    end_dt_wrapper_obj = DateTimeWrapper('5/25/22, 11:39:17 AM', FORMAT_SHORT_YEAR)
    hh, mm, ss = calculate_duration_hh_mm_ss(start_dt_w_obj, end_dt_wrapper_obj)
    assert (hh,mm,ss) == (1, 8, 24), "No values equals"
    