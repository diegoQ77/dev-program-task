FORMAT_SHORT_YEAR = '%m/%d/%y, %I:%M:%S %p'
FORMAT_LONG_YEAR = '%m/%d/%Y, %I:%M:%S %p'

import pytest
from src.teams_meeting import  Participant
from src.helpers import DateTimeWrapper

def test_create_participant():
    first_join = DateTimeWrapper("6/17/22, 10:29:37 AM")
    last_leave = DateTimeWrapper("6/17/22, 12:20:35 PM")
    p = Participant("Maria Marquina Guevara", "Maria.Marquina@fundacion-jala.org", first_join, last_leave, "Organizer")
    assert p.in_meeting_duration is not None
    assert p.in_meeting_duration.hours is not None and p.in_meeting_duration.hours >=0
    assert p.in_meeting_duration.minutes is not None and p.in_meeting_duration.minutes >=0
    assert p.in_meeting_duration.seconds is not None and p.in_meeting_duration.seconds >=0
    first_join = DateTimeWrapper("10/18/2022, 5:50:03 PM", FORMAT_LONG_YEAR)
    last_leave = DateTimeWrapper("10/18/2022, 7:53:52 PM", FORMAT_LONG_YEAR)
    p = Participant("Salome Quispe Guarachi", "Salome.Quispe@fundacion-jala.org", first_join, last_leave, "Presenter")
    assert p.in_meeting_duration is not None
    assert p.in_meeting_duration.hours is not None and p.in_meeting_duration.hours >=0
    assert p.in_meeting_duration.minutes is not None and p.in_meeting_duration.minutes >=0
    assert p.in_meeting_duration.seconds is not None and p.in_meeting_duration.seconds >=0
    
    
    