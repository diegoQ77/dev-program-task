from src.helpers import calculate_duration_hh_mm_ss
import datetime

FORMAT_SHORT_YEAR = '%m/%d/%y, %I:%M:%S %p'
FORMAT_LONG_YEAR = '%m/%d/%Y, %I:%M:%S %p'

class Duration:
    """
    The Duration class represents a time value like 1h 32m 38s
    """

    def __init__(self, hours, minutes, seconds):
        self.__hours = hours
        self.__minutes = minutes
        self.__seconds = seconds

    @property
    def hours(self):
        return self.__hours

    @property
    def minutes(self):
        return self.__minutes

    @property
    def seconds(self):
        return self.__seconds
    
    def __str__(self):
        """ 
        This method overwrites default implementation to return `Duration` object as str in this format 1h 32m 38s
        """
        str_format_time = str(datetime.timedelta(seconds= self.get_total_seconds()))
        str_format_time = str_format_time.split(":")
        hour = "" if str_format_time[0] == '0' else f"{int(str_format_time[0])}h"
        minutes = "" if str_format_time[1] == '00' else f"{int(str_format_time[1])}m"
        seconds = "" if str_format_time[2] == '00' else f"{int(str_format_time[2])}s"
        str_final_format = f"{hour} {minutes} {seconds}"
        return str_final_format.strip()
        
    def __eq__(self, other):
        return True if isinstance(other, Duration) and self.get_total_seconds() == other.get_total_seconds() else False
    
    def __lt__(self, other):
        return True if self.get_total_seconds() < other.get_total_seconds() else False
    
    def __le__(self, other):
        return True if self.get_total_seconds() <= other.get_total_seconds() else False
    
    def __gt__(self, other):
        return True if self.get_total_seconds() > other.get_total_seconds() else False
    
    def __ge__(self, other):
        return True if self.get_total_seconds() >= other.get_total_seconds() else False
    
    def get_total_seconds(self):
        return self.__hours * 3600 + self.__minutes * 60 + self.__seconds


class Participant:
    """
    The class Participant repressents an attendee in a meeting ocurrence
    """
    
    def __init__(self, name, email, first_join, last_leave, role):
        """Create a new Participant object

        :param name: The name of the participant, str object
        :param email: The email of the participant, str object
        :param first_join: DateTimeWrapper object
        :param last_leave: DateTimeWrapper object
        param role: str object
        """

        self.__name = name
        self.__email = email 
        self.__first_join = first_join
        self.__last_leave = last_leave 
        self.__role = role

    @property
    def in_meeting_duration(self):
        """
        This is the property that gets the in-meeting duration
        :return: `Duration` object, the elapsed time between :attr:`__first_join` and :attr:`__last_leave`
        """
        hh, mm, ss = calculate_duration_hh_mm_ss(self.__first_join, self.__last_leave)
        return Duration(hh, mm, ss)

    @property
    def name(self):
        return self.__name

    @property
    def email(self):
        return self.__email

    @property
    def first_join(self):
        return self.__first_join

    @property
    def last_leave(self):
        return self.__last_leave

    @property
    def role(self):
        return self.__role


class Meeting:
    """
    The Meeting represents a Team's meeting
    """

    def __init__(self, title):
        """
        Create a new Meetig object.
        :param title: The title of the meeting
        """
        self.__title = title
        self.__attendance = []

    @property
    def title(self):
        return self.__title

    @property
    def attendance(self):
        """ This property gets the list of meeting ocurrences
        :return: The value of :attr:`__attendance`
        """
        return self.__attendance

    def add_attendance(self, meeting_ocurrences):
        """ 
        This method adds 1 or more meeting ocurrences in the :attr:`__attendance`
        if any `MeetingOcurrence.start_time` and `MeetingOcurrence.end_time` exists already in the :attr:`__attendance` 
        then update all MeetingOcurrence attributes except `MeetingOcurrence.start_time` and `MeetingOcurrence.end_time`
        :param meeting_ocurrences: A list containing 1 or more `MeetingOcurrence` objects
        """
        are_equals = False
        for meeting in meeting_ocurrences:
            for meetin_class in self.__attendance:
                if meeting.start_time.datetime ==  meetin_class.start_time.datetime and meeting.end_time.datetime == meetin_class.end_time.datetime:
                    meetin_class.attended_setter(meeting.attended_setter)
                    meetin_class.add_participants(meetin_class.participants)
                    are_equals = True
            if are_equals == False:
                self.__attendance.append(meeting)


class MeetingOcurrence:
    """
    The MeetingOcurrence class represents a Team's meeting ocurrence
    """

    def __init__(self, attended, start_time, end_time):
        """
        Create a new MeetingOcurrence.

        :param attended: The number of participants
        :param start_time: The meeting start time, DateTimeWrapper object
        :paran end_time: The meeting end time, DateTimeWrapper object
        """
        self.__attended = attended
        self.__start_time = start_time
        self.__end_time = end_time
        self.__participants = []

    @property
    def meeting_duration(self):
        """
        This is the property that gets the meeting duration
        :return: The meeting duration, elapsed time between :attr:`__start_time` and :attr:`__end_time` as `Duration` object
        """
        hh,mm,ss = calculate_duration_hh_mm_ss(self.__start_time, self.__end_time)
        duration_obj = Duration(hh,mm,ss)
        return duration_obj
    
    def attended_setter(self, new_attended):
        print("setter method new value:   ", new_attended)
        self.__attended = new_attended

     
    @property
    def start_time(self):
        """ This property returns the list of participants
        :return: The value of :attr:`__participants`
        """
        return self.__start_time
    
    @property
    def end_time(self):
        """ This property returns the list of participants
        :return: The value of :attr:`__participants`
        """
        return self.__end_time
    
    @property
    def participants(self):
        """ This property returns the list of participants
        :return: The value of :attr:`__participants`
        """
        return self.__participants

    def add_participants(self, participants):
        """ 
        This method adds 1 or more participants in the :attr:`__participants`
        if `Participant.email` exists already update all Participant attributes except `Participant.email`
        :param participants: A list containing 1 or more `Participant` objects
        """
        are_equals = False
        for new_obj_participant in participants:
            for old_p in self.__participants:
                if new_obj_participant.email == old_p.email:
                    are_equals = True
            if are_equals == False:
                self.__participants.append(new_obj_participant)